package com.netcracker.airport.service.exception;

public class PilotException  extends Exception{
    public PilotException(String errorMessage){
        super(errorMessage);
    }
}
