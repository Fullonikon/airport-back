package com.netcracker.airport.service.repository;

import com.netcracker.airport.service.dto.Pilot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface PilotRepository extends JpaRepository<Pilot, Long> {
    Pilot findByLogin(String login);

    Pilot findByLoginAndPassword(String login, String password);
}
