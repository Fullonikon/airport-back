package com.netcracker.airport.service.repository;

import com.netcracker.airport.service.dto.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
}
