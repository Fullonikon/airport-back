package com.netcracker.airport.service.dto.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record AuthResponse(@JsonProperty String accessToken) {
    @JsonCreator
    public AuthResponse {

    }
}
