package com.netcracker.airport.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "pilot")
public class Pilot {
    @Id
    @GeneratedValue
    private long employeeId;
    private String firstName;
    private String lastName;
    private String category;
    private String login;
    private String password;
    private String role;

    @OneToMany(mappedBy = "pilot", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Schedule> schedule;

    public Pilot() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Set<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(Set<Schedule> schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pilot pilot = (Pilot) o;
        return employeeId == pilot.employeeId &&
                Objects.equals(firstName, pilot.firstName) &&
                Objects.equals(lastName, pilot.lastName) &&
                Objects.equals(category, pilot.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, firstName, lastName, category);
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "employeeId=" + employeeId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
