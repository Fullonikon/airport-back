package com.netcracker.airport.service.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.netcracker.airport.service.dto.Schedule;

public record ScheduleRequest(@JsonProperty Schedule schedule, @JsonProperty String tailNumber, @JsonProperty Long employeeId) {
    @JsonCreator
    public ScheduleRequest {
    }
}
