package com.netcracker.airport.service.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.netcracker.airport.service.dto.Transport;

public record SaveTransportRequest(@JsonProperty Transport transport, @JsonProperty Long hangarId) {
    @JsonCreator
    public SaveTransportRequest {
    }
}
