package com.netcracker.airport.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "hangar")
public class Hangar {
    @Id
    @GeneratedValue
    @Column(name="hangarId")
    private long hangarId;

    private String color;

    private String hangarType;

    @OneToMany
    @JoinColumn(name = "hangar_id")
    @JsonIgnore
    private Set<Transport> transport;

    public Hangar() {
    }

    public void setHangarId(long hangarId) {
        this.hangarId = hangarId;
    }

    public long getHangarId() {
        return hangarId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHangarType() {
        return hangarType;
    }

    public void setHangarType(String hangarType) {
        this.hangarType = hangarType;
    }

    public Set<Transport> getTransport() {
        return transport;
    }

    public void setTransport(Set<Transport> transport) {
        this.transport = transport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hangar hangar = (Hangar) o;
        return hangarId == hangar.hangarId &&
                Objects.equals(color, hangar.color) &&
                Objects.equals(hangarType, hangar.hangarType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hangarId, color, hangarType);
    }

    @Override
    public String toString() {
        return "Hangar{" +
                "hangarId=" + hangarId +
                '}';
    }
}
