package com.netcracker.airport.service.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "transport")
public class Transport {
    private String type;
    private int capacity;
    private int flightRange;
    private String model;

    @Id
    private String tailNumber;

    @ManyToOne
    @JoinColumn(name = "hangar_id")
    private Hangar hangar;

    @OneToMany(mappedBy="transport", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Schedule> schedules;

    public Transport() {
    }

    public void setTailNumber(String tailNumber) {
        this.tailNumber = tailNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTailNumber() {
        return tailNumber;
    }

    public Hangar getHangar() {
        return hangar;
    }

    public void setHangar(Hangar hangar) {
        this.hangar = hangar;
    }

    public Set<Schedule> getSchedule() {
        return schedules;
    }

    public void setSchedule(Set<Schedule> schedule) {
        this.schedules = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transport transport = (Transport) o;
        return capacity == transport.capacity &&
                flightRange == transport.flightRange &&
                Objects.equals(tailNumber, transport.tailNumber) &&
                Objects.equals(type, transport.type) &&
                Objects.equals(model, transport.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, capacity, flightRange, model, tailNumber);
    }

    @Override
    public String toString() {
        return "Transport{" +
                "tailNumber=" + tailNumber +
                '}';
    }
}
