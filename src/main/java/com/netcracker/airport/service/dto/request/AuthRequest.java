package com.netcracker.airport.service.dto.request;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record AuthRequest(@JsonProperty String login, @JsonProperty String password) {
    @JsonCreator
    public AuthRequest {

    }
}
