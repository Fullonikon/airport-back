package com.netcracker.airport.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "schedule")
public class Schedule {
    @Id
    @GeneratedValue
    private long flightId;

    private Date departureTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tail_number")
    @JsonIgnore
    private Transport transport;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="employee_id")
    //@JsonIgnore
    private Pilot pilot;

    public Schedule() {
    }

    public void setFlightId(long flightId) {
        this.flightId = flightId;
    }

    public long getFlightId() {
        return flightId;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Pilot getPilot() {
        return pilot;
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return flightId == schedule.flightId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightId, departureTime);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "flightId=" + flightId +
                ", departureTime=" + departureTime +
                '}';
    }
}
