package com.netcracker.airport.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record RealPilot(@JsonProperty long employeeId, @JsonProperty String firstName, @JsonProperty String lastName,
                        @JsonProperty String category, @JsonProperty String login, @JsonProperty String role) {
    @JsonCreator
    public RealPilot {
    }

    public static RealPilot of(Pilot pilot) {
        return new RealPilot(pilot.getEmployeeId(), pilot.getFirstName(), pilot.getLastName(), pilot.getCategory(), pilot.getLogin(), pilot.getRole());
    }
}
