package com.netcracker.airport.service.service;

import com.netcracker.airport.service.dto.Hangar;
import com.netcracker.airport.service.dto.Transport;
import com.netcracker.airport.service.dto.request.SaveTransportRequest;
import com.netcracker.airport.service.repository.HangarRepository;
import com.netcracker.airport.service.repository.TransportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

/**
 * Все методы для работы с транспортом
 */
@Service
public class TransportService {

    private final TransportRepository transportRepository;
    private final HangarRepository hangarRepository;

    @Autowired
    public TransportService(TransportRepository transportRepository, HangarRepository hangarRepository) {
        this.transportRepository = transportRepository;
        this.hangarRepository = hangarRepository;
    }

    public Iterable<Transport> findAll() {
        return this.transportRepository.findAll();
    }

    public Transport saveTransport(SaveTransportRequest request) {
        if (request == null) throw new NoSuchElementException("Bad request");
        Hangar hangar = this.hangarRepository.getOne(request.hangarId());
        Transport transport = request.transport();
        transport.setHangar(hangar);
        return this.transportRepository.save(transport);
    }

    public void deleteTransport(String id){
        if (id == null) throw new NoSuchElementException("Bad request");
        this.transportRepository.deleteById(id);
    }

}
