package com.netcracker.airport.service.service;

import com.netcracker.airport.service.dto.Hangar;
import com.netcracker.airport.service.dto.Pilot;
import com.netcracker.airport.service.dto.Schedule;
import com.netcracker.airport.service.dto.Transport;
import com.netcracker.airport.service.dto.request.ScheduleRequest;
import com.netcracker.airport.service.repository.PilotRepository;
import com.netcracker.airport.service.repository.ScheduleRepository;
import com.netcracker.airport.service.repository.TransportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Все методы для работы с расписанием
 */
@Service
public class ScheduleService {
    private final ScheduleRepository scheduleRepository;
    private final PilotRepository pilotRepository;
    private final TransportRepository transportRepository;

    @Autowired
    public ScheduleService(ScheduleRepository scheduleRepository, PilotRepository pilotRepository, TransportRepository transportRepository) {
        this.scheduleRepository = scheduleRepository;
        this.pilotRepository = pilotRepository;
        this.transportRepository = transportRepository;
    }

    public Schedule schedule(ScheduleRequest request) {
        Pilot pilot = pilotRepository.getOne(request.employeeId());
        Transport transport = transportRepository.getOne(request.tailNumber());
        Schedule schedule = request.schedule();
        schedule.setPilot(pilot);
        schedule.setTransport(transport);
        return this.scheduleRepository.save(schedule);
    }

    public Iterable<Schedule> findAll() {
        return this.scheduleRepository.findAll();
    }
}
