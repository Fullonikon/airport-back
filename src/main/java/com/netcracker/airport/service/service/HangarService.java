package com.netcracker.airport.service.service;

import com.netcracker.airport.service.dto.Hangar;
import com.netcracker.airport.service.repository.HangarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Все методы для работы с ангарами
 */
@Service
public class HangarService {

    private final HangarRepository hangarRepository;

    @Autowired
    public HangarService(HangarRepository hangarRepository) {
        this.hangarRepository = hangarRepository;
    }

    public Iterable<Hangar> findAll() {
        return this.hangarRepository.findAll();
    }

    public Iterable<Hangar> findHangarsByType(String type) {
        return this.hangarRepository.findByHangarType(type);
    }

    public Hangar saveHangar(Hangar hangar) {
        return  this.hangarRepository.save(hangar);
    }

    public void deleteHangar(Long id){
        this.hangarRepository.deleteById(id);
    }
}