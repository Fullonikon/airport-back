package com.netcracker.airport.service.service;

import com.netcracker.airport.service.detail.CustomUserDetails;
import com.netcracker.airport.service.dto.Pilot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private PilotService userService;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Pilot userEntity = userService.findByLogin(username);
        return isNull(userEntity)
            ? null
            : CustomUserDetails.fromUserEntityToCustomUserDetails(userEntity);
    }
}