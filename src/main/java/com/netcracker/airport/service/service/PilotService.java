package com.netcracker.airport.service.service;

import com.netcracker.airport.service.dto.Pilot;
import com.netcracker.airport.service.dto.RealPilot;
import com.netcracker.airport.service.exception.PilotException;
import com.netcracker.airport.service.repository.PilotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;


/**
 * Все методы для работы с пилотами
 */
@Service
public class PilotService {

    private final PilotRepository pilotRepository;

    @Autowired
    public PilotService(PilotRepository pilotRepository) {
        this.pilotRepository = pilotRepository;
    }

    /**
     * Находит ВСЕХ пользователей в базе данных
     * @return
     */
    public Iterable<RealPilot> findAll() {
        return this.pilotRepository.findAll()
                .stream()
                .map(RealPilot::of)
                .collect(Collectors.toList());
    }

    /**
     * Находит пользователей, у которых есть роль пилота (исключаем админов)
     * @return
     */
    public Iterable<RealPilot> findAllPilots() {
        return this.pilotRepository.findAll()
                .stream()
                .filter(pilot -> pilot.getRole().equals("PILOT"))
                .map(RealPilot::of)
                .collect(Collectors.toList());
    }

    public Pilot savePilot(Pilot pilot) throws PilotException {
        if (findByLogin(pilot.getLogin()) == null)
            return this.pilotRepository.save(pilot);
        throw new PilotException("Pilot already exists");
    }

    public Pilot findByLogin(String login) {
        return this.pilotRepository.findByLogin(login);
    }

    /**
     * Для проверки аутентификации
     * @param login
     * @param password
     * @return
     */
    public Pilot findByLoginAndPassword(String login, String password) {
        return this.pilotRepository.findByLoginAndPassword(login, password);
    }

    public void deletePilot(Long id) {
        this.pilotRepository.deleteById(id);
    }

}
