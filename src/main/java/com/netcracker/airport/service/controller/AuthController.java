package com.netcracker.airport.service.controller;

import com.netcracker.airport.service.jwt.JwtProvider;
import com.netcracker.airport.service.dto.Pilot;
import com.netcracker.airport.service.dto.request.AuthRequest;
import com.netcracker.airport.service.dto.response.AuthResponse;
import com.netcracker.airport.service.service.PilotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

import static com.netcracker.airport.service.configuration.Addresses.MAIN_LINK;
import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = {MAIN_LINK})
public class AuthController {

    @Autowired
    private PilotService pilotService;

    @Autowired
    private JwtProvider jwtProvider;

    /**
     * При запросе, проверяем совпадение логина и пароля в базе, при успехе выдаём соотвествующий токен
     * @param request
     * @return
     */
    @PostMapping("/auth")
    public ResponseEntity<AuthResponse> auth(@RequestBody AuthRequest request) {
        Pilot pilot = pilotService.findByLoginAndPassword(request.login(), request.password());
        if (isNull(pilot)) return ResponseEntity.status(403).build();
        String token = jwtProvider.generateToken(pilot.getLogin(), new HashMap<>() {{ put("role", pilot.getRole()); }});
        return ResponseEntity.ok(new AuthResponse(token));
    }
}