package com.netcracker.airport.service.controller;

import com.netcracker.airport.service.dto.Hangar;
import com.netcracker.airport.service.dto.Pilot;
import com.netcracker.airport.service.dto.RealPilot;
import com.netcracker.airport.service.dto.Transport;
import com.netcracker.airport.service.dto.request.SaveTransportRequest;
import com.netcracker.airport.service.service.HangarService;
import com.netcracker.airport.service.service.PilotService;
import com.netcracker.airport.service.service.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

import static com.netcracker.airport.service.configuration.Addresses.MAIN_LINK;

/**
 * Все запросы от лица админа
 */
@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = {MAIN_LINK})
public class AdminController {
    private final TransportService transportService;
    private final PilotService pilotService;
    private final HangarService hangarService;

    @Autowired
    public AdminController(TransportService transportService, PilotService pilotService,
                          HangarService hangarService) {
        this.transportService = transportService;
        this.pilotService = pilotService;
        this.hangarService = hangarService;
    }

    @PostMapping("/saveTransport")
    public ResponseEntity<Transport> saveTransport(@RequestBody SaveTransportRequest request) {
        try{
            return ResponseEntity.ok(this.transportService.saveTransport(request));
        }
        catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/savePilot")
    public ResponseEntity<?> savePilot(@RequestBody Pilot pilot) {
        try {
            return ResponseEntity.ok(this.pilotService.savePilot(pilot));
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/findAllPilotsWithAdmins")
    public Iterable<RealPilot> findAllPilotsWithAdmins() {
        return this.pilotService.findAll();
    }

    @PostMapping("/saveHangar")
    public ResponseEntity<Hangar> saveHangar(@RequestBody Hangar hangar) {
        try {
            return ResponseEntity.ok(this.hangarService.saveHangar(hangar));
        }
        catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/deleteHangar")
    public ResponseEntity deleteHangar(@PathParam("id") Long id) {
        try {
            this.hangarService.deleteHangar(id);
            return ResponseEntity.ok().build();
        }
        catch(Exception e){
            return ResponseEntity.badRequest().build();
        }

    }

    @DeleteMapping("/deleteTransport")
    public ResponseEntity deleteTransport(@PathParam("id") String id) {
        try {
            this.transportService.deleteTransport(id);
            return ResponseEntity.ok().build();
        }
        catch(Exception e){
            return ResponseEntity.badRequest().build();
        }

    }

    @DeleteMapping("/deletePilot")
    public ResponseEntity deletePilot(@PathParam("id") Long id) {
        this.pilotService.deletePilot(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/findHangarsByType")
    public Iterable<Hangar> findHangarsByType(@PathParam("type") String type) {
        return this.hangarService.findHangarsByType(type);
    }

}
