package com.netcracker.airport.service.controller;

import com.netcracker.airport.service.dto.*;
import com.netcracker.airport.service.dto.request.ScheduleRequest;
import com.netcracker.airport.service.service.HangarService;
import com.netcracker.airport.service.service.ScheduleService;
import com.netcracker.airport.service.service.TransportService;
import com.netcracker.airport.service.service.PilotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.netcracker.airport.service.configuration.Addresses.MAIN_LINK;

/**
 * Все запросы, доступные простым пилотам
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {MAIN_LINK})
public class MainController {

    private final TransportService transportService;
    private final PilotService pilotService;
    private final HangarService hangarService;
    private final ScheduleService scheduleService;

    @Autowired
    public MainController(TransportService transportService, PilotService pilotService,
                          HangarService hangarService, ScheduleService scheduleService) {
        this.transportService = transportService;
        this.pilotService = pilotService;
        this.hangarService = hangarService;
        this.scheduleService = scheduleService;
    }

    @GetMapping("/findAllTransports")
    public Iterable<Transport> findAllTransports() {
        return this.transportService.findAll();
    }

    @GetMapping("/findAllHangars")
    public Iterable<Hangar> findAllHangars() {
        return this.hangarService.findAll();
    }

    @GetMapping("/findAllPilots")
    public Iterable<RealPilot> findAllPilots() {
        return this.pilotService.findAllPilots();
    }

    @GetMapping("/findAllSchedules")
    public Iterable<Schedule> findAllSchedules() { return this.scheduleService.findAll(); }

    @PostMapping("/schedule")
    public Schedule schedule(@RequestBody ScheduleRequest schedule) {
        return this.scheduleService.schedule(schedule);
    }

}
